package com.etone.etonesso.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
/**
 * 数据源配置
 */
public class DatasourceConfig {

    @Bean
    public DataSource dataSource(){
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setUsername("root");
        hikariDataSource.setPassword("root");
        hikariDataSource.setDriverClassName("org.gjt.mm.mysql.Driver");
        hikariDataSource.setJdbcUrl("jdbc:mysql://localhost:3306/test");
        return hikariDataSource;
    }
}
