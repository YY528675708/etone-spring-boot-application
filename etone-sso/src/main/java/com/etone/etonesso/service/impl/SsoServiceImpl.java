package com.etone.etonesso.service.impl;

import com.alibaba.fastjson.JSON;
import com.etone.etonecommon.exception.BusinessException;
import com.etone.etonecommon.util.HttpClientUtil;
import com.etone.etonecommon.vo.EtoneResult;
import com.etone.etonesso.dao.UserMapper;
import com.etone.etonesso.enetity.User;
import com.etone.etonesso.service.SsoService;
import com.etone.etonesso.util.RedisUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

@Service
public class SsoServiceImpl implements SsoService {
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private UserMapper userMapper;

    private Logger logger = LoggerFactory.getLogger(SsoServiceImpl.class);
    private HttpClientUtil httpClientUtil = new HttpClientUtil();

    @Override
    public EtoneResult<String> login(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        User user = new User(username, password);
        User ucopy = userMapper.get(user);
        if (null!=ucopy){
            String key = UUID.randomUUID().toString().replace("-", "");
            boolean set = redisUtil.set(key, JSON.toJSONString(ucopy));
            if (set){
                logger.info("登录成功，key："+key);
                Cookie cookie = new Cookie("token",key);
                response.addCookie(cookie);
                String url = request.getParameter("url");
                httpClientUtil.getResult(url,new HashMap<>());
                return new EtoneResult<>(0,"ok",null);
            } else {
                throw new BusinessException("redis存储故障！",102);
            }
        } else {
            throw new BusinessException("用户名或密码错误",101);
        }
    }

    @Override
    public EtoneResult<String> check(HttpServletRequest request) {
        String token = request.getParameter("token");
        if (null != token){
            String s = redisUtil.get(token);
            if (null != s){
                //更新超时时间
                redisUtil.setExpire(token);
                return new EtoneResult<>(0,"token校验通过！",null);
            } else {
                return new EtoneResult<>(0,"token已过期，请重新登陆",null);
            }
        } else {
            return new EtoneResult<>(1,"无token，请跳转到sso系统登录！",null);
        }
    }

    @Override
    public EtoneResult<String> logout(HttpServletRequest request) {
        String token = request.getParameter("token");
        if (null != token){
            String s = redisUtil.get(token);
            if (null != s){
                //更新超时时间
                redisUtil.delete(token);
                return new EtoneResult<>(0,"用户已退出！",null);
            } else {
                return new EtoneResult<>(0,"token已过期",null);
            }
        } else {
            return new EtoneResult<>(1,"无token！",null);
        }
    }
}
