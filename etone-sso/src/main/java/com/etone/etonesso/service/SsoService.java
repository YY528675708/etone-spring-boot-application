package com.etone.etonesso.service;


import com.etone.etonecommon.vo.EtoneResult;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface SsoService {
    EtoneResult<String> login(HttpServletRequest request, HttpServletResponse response) throws IOException;

    EtoneResult<String> check(HttpServletRequest request);

    EtoneResult<String> logout(HttpServletRequest request);
}
