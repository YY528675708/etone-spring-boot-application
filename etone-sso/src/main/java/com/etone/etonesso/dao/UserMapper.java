package com.etone.etonesso.dao;

import com.etone.etonesso.enetity.User;

public interface UserMapper {
    User get(User user);
}
