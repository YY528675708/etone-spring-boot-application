package com.etone.etonesso.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DoorController {
    @GetMapping("sso/login")
    public String ssoLogin(){
        return "/login.html";
    }
}
