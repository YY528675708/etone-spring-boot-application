package com.etone.etonesso.controller;

import com.etone.etonecommon.vo.EtoneResult;
import com.etone.etonesso.service.SsoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class SsoController {
    @Autowired
    private SsoService ssoService;

    @RequestMapping("login")
    public EtoneResult login(HttpServletRequest request, HttpServletResponse response){
        return ssoService.login(request, response);
    }

    @RequestMapping("check")
    public EtoneResult check(HttpServletRequest request){
        return ssoService.check(request);
    }

    @RequestMapping("logout")
    public EtoneResult logout(HttpServletRequest request){
        return ssoService.logout(request);
    }


}
