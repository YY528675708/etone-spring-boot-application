package com.etone.etonesso;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.etone.etonesso.dao")
public class EtoneSsoApplication {

    public static void main(String[] args) {
        SpringApplication.run(EtoneSsoApplication.class, args);
    }

}
