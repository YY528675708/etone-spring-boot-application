package com.etone.etoneapplication.util;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @description:        获取方法执行时间长
 * @author:Mr.Young
 * @date:2018/10/25
 */
@Component
@Aspect
public class TimeAspect {

    @Pointcut("execution(public * com.etone.etoneapplication.service.*.*(..))")
    public void log(){}

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(TimeAspect.class);

    //统计请求的处理时间
    ThreadLocal<Long> startTime = new ThreadLocal<>();

    @Before("log()")
    public void doBefore(JoinPoint joinPoint) throws Throwable{
        startTime.set(System.currentTimeMillis());
        //接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        //记录请求的内容
        logger.info("Aspect_URL:"+request.getRequestURL().toString());
        logger.info("Aspect_Method:"+request.getMethod());
    }

    @AfterReturning(returning = "ret" , pointcut = "log()")
    public void doAfterReturning(Object ret){
        //处理完请求后，返回内容
        logger.info("方法执行时间:"+ (System.currentTimeMillis() - startTime.get())+"ms");
    }
}
