package com.etone.etoneapplication.filter;

import com.etone.etonecommon.util.HttpClientUtil;
import com.etone.etonecommon.vo.EtoneResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 登录拦截器
 */
@Component
public class LoginFilter implements HandlerInterceptor {

    private HttpClientUtil httpClientUtil = new HttpClientUtil();
    private Logger logger = LoggerFactory.getLogger(LoginFilter.class);

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

    //这个方法是在访问接口之前执行的，我们只需要在这里写验证登陆状态的业务逻辑，就可以在用户调用指定接口之前验证登陆状态了
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Cookie[] cookies = request.getCookies();
        Map<String,String> map = new HashMap<>();
        String url = request.getRequestURL().toString();
        if (null != cookies && cookies.length > 0){
            List<Cookie> token = Arrays.stream(cookies).filter(s -> s.getName().equals("token")).collect(Collectors.toList());
            if (token.size() == 1){
                Cookie cookie = token.get(0);
                String value = cookie.getValue();
                logger.info("请求有token："+value);
                map.put("token",value);
                EtoneResult<String> result = httpClientUtil.getResult("localhost:7006/check", map);
                return result.getCode() == 0;
            } else {
                logger.info("有多个token！！！");
                map.put("url",url);
                EtoneResult<String> result = httpClientUtil.getResult("localhost:7006/sso/login", map);
                return false;
            }
        } else {
            logger.info("该请求无token！！！");
            map.put("url",url);
            EtoneResult<String> result = httpClientUtil.getResult("localhost:7006/sso/login", map);
            return false;
        }
    }

}
