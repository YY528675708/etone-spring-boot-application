package com.etone.etoneapplication;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.etone.etoneapplication.dao")
public class EtoneApplication {

    public static void main(String[] args) {
        SpringApplication.run(EtoneApplication.class, args);
    }

}
