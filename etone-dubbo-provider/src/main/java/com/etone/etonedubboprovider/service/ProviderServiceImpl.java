package com.etone.etonedubboprovider.service;

import com.alibaba.dubbo.config.annotation.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.ProviderService;

@Service
@org.springframework.stereotype.Service
public class ProviderServiceImpl implements ProviderService {
    private Logger logger = LoggerFactory.getLogger(ProviderServiceImpl.class);

    @Override
    public String provider(String serviceName) {
        logger.info("开始调用服务："+serviceName);
        return "数据处理完成，结果为："+serviceName;
    }
}
