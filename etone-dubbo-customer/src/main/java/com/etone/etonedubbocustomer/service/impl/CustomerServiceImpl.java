package com.etone.etonedubbocustomer.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.etone.etonedubbocustomer.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import service.ProviderService;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Reference
    ProviderService service;

    private Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

    @Override
    public String customer(String serviceName) {
        logger.info("服务消费者开始消费服务！");
        return  service.provider(serviceName);
    }
}
