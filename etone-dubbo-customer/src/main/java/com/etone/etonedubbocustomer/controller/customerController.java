package com.etone.etonedubbocustomer.controller;

import com.etone.etonedubbocustomer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class customerController {
    @Autowired
    private CustomerService service;

    @GetMapping("dubbo/customer")
    public String customer(String serviceName){
        return "经过了服务提供者处理的数据："+ service.customer(serviceName);
    }
}
