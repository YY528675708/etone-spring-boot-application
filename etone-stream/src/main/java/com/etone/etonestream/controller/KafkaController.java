package com.etone.etonestream.controller;

import com.etone.etonestream.service.KafkaOutput;
import com.etone.etonestream.service.ThreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class KafkaController {
    @Autowired
    private KafkaOutput kafkaOutput;
    @Autowired
    private ThreadService threadService;

    @GetMapping("kafka")
    public String sendMsg(String topic, String msg){
        kafkaOutput.send(topic,msg);
        return "ok";
    }

    @GetMapping("test")
    public String sendMsg(){
        List<String> list = Arrays.asList("12","13","14","15");
        threadService.show(list);
        return "ok";
    }
}
