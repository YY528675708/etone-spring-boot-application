package com.etone.etonestream.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaOutput {
    @Autowired
    private KafkaTemplate<String, String> template;

    private Logger logger = LoggerFactory.getLogger(KafkaOutput.class);


    /**
     * 发送消息
     * @param topic     标签
     * @param msg       信息
     */
    public void send(String topic,String msg){
        logger.info("生产者:"+msg);
        template.send(topic, msg);
    }
}
