package com.etone.etonestream.service;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class ThreadService {

    private static ExecutorService service = Executors.newFixedThreadPool(300);     //200个线程处理网元

    public void show(List<String> strings){
        CountDownLatch latch = new CountDownLatch(strings.size());
        for (int i = 0; i<strings.size();i++){
            String s = strings.get(i);
            int z = i;
            service.execute(new Runnable() {
                @Override
                public void run() {
                    System.out.println("第"+z+"次打印："+s);
                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    latch.countDown();
                }
            });
        }
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("都打印完了！！！");
    }

}
