package com.etone.etonestream.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;

//@Service
public class KafkaInput {

    private Logger logger = LoggerFactory.getLogger(KafkaInput.class);

    @KafkaListener(topics = "topic-test")
    public void listen(ConsumerRecord<String, String> cr) throws Exception {
        logger.info("消费者:"+cr.value());
    }
}
