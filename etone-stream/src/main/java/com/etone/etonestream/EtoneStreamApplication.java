package com.etone.etonestream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EtoneStreamApplication {
    public static void main(String[] args) {
        SpringApplication.run(EtoneStreamApplication.class, args);
    }
}
