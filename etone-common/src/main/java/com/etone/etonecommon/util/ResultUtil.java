package com.etone.etonecommon.util;


import com.etone.etonecommon.entity.Result;
import com.etone.etonecommon.exception.ExceptionEnum;

/**
 * 响应返回Result  工具类
 */
public class ResultUtil {

    /**
     * 返回成功，传入返回体具体出參
     * @param object
     * @return
     */
    public static Result success(Object object){
        Result result = new Result();
        result.setCode(0);
        result.setResult("成功");
        result.setContent(object);
        return result;
    }

    /**
     * 提供给部分不需要出參的接口
     * @return
     */
    public static Result success(){
        return success(null);
    }

    /**
     * 自定义错误信息
     * @param code
     * @param msg
     * @return
     */
    public static Result error(Integer code,String msg,String errorStack){
        Result result = new Result();
        result.setCode(code);
        result.setResult(msg);
        result.setErrorStrack(errorStack);
        return result;
    }

    /**
     * 返回异常信息，在已知的范围内
     * @param exceptionEnum
     * @return
     */
    public static Result error(ExceptionEnum exceptionEnum, String errorStack){
        Result result = new Result();
        result.setCode(exceptionEnum.getCode());
        result.setResult(exceptionEnum.getMsg());
        result.setErrorStrack(errorStack);
        return result;
    }
}
