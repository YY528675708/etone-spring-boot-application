package com.etone.etonecommon.util;

import java.util.List;

/**
 * 分页用接口
 * 函数式接口，只有一个方法
 */
public interface GetPageDataInterface<T> {
    /**
     * 返回一个List
     */
    List<T> getPageData();
}
