package com.etone.etonecommon.util;

import com.etone.etonecommon.entity.PageResultVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import java.util.List;

public class PageUtil {
    public static  <T> PageResultVo genPageResultVo(GetPageDataInterface<T> getPageDataInterface, int pageNumber,
                                                    int pageSize) {
        PageHelper.startPage(pageNumber, pageSize);
        List<T> result = getPageDataInterface.getPageData();
        Page page = (Page) result;
        PageResultVo<List<T>> resultVo = new PageResultVo();
        resultVo.setPageNumber(page.getPageNum());
        resultVo.setPageSize(page.getPageSize());
        resultVo.setTotal(page.getTotal());
        resultVo.setCode(0);
        resultVo.setContent(result);
        resultVo.setResult("成功");
        return resultVo;
    }
}
