package com.etone.etonecommon.exception;

import com.etone.etonecommon.entity.Result;
import com.etone.etonecommon.util.ResultUtil;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * SPring统一异常处理
 */

@ControllerAdvice
public class ExceptionHandle {

    private final static Logger LOGGER = LoggerFactory.getLogger(ExceptionHandle.class);

    /**
     * 判断错误是否是已定义的已知错误，不是则由未知错误代替，同时记录在log中
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result exceptionGet(Exception e){
        e.printStackTrace();
        if(e instanceof DescribeException){
            DescribeException exception = (DescribeException) e;
            return ResultUtil.error(exception.getCode(),exception.getMessage(), ExceptionUtils.getStackTrace(e));
        }
        if(e instanceof BusinessException) {
            LOGGER.error("业务异常："+e.getMessage());
            BusinessException businessException = (BusinessException)e;
            return ResultUtil.error(businessException.getCode(), businessException.getMessage(), ExceptionUtils.getStackTrace(e));
        }

        //未知错误
        LOGGER.error("【系统异常】{}",e);
        return ResultUtil.error(ExceptionEnum.UNKNOW_ERROR, ExceptionUtils.getStackTrace(e));
    }
}