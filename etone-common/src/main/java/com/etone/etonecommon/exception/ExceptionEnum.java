package com.etone.etonecommon.exception;

/**
 * 一个枚举类，来记录一些我们已知的错误信息，可以在代码中直接使用。
 */
public enum ExceptionEnum {

    UNKNOW_ERROR(-1, "未知错误"),
    ERROR(-2,"失败"),
    TOKEN_NOT_FOUND(-101,"该Token不存在或已失效"),

    PARAM_CHECK_ERROR(-105,"参数校验失败"),
    JSON_FORMAT_ERROR(-106,"JSON解析错误"),
    AUDITING_STATUS_ERROR(-107,"审核状态不符合逻辑"),
    OPEN_STATUS_ERROR(-108,"启用状态不符合逻辑"),
    PROCEDURE_ID_NOT_FOUND(-108,"该流程ID不存在"),
    STARTTIME_SHOULD_BEHIND_NOW(-109,"开始时间必须大于当前时间"),
    CANNOT_FIND_LOOP_GROUP_NODE(-110,"找不到对应的循环节点(循环开始或者循环结束)"),
    LOOP_TIME_OVER_MAX(-111,"循环次数超过1000次，异常退出"),
    JS_PARSE_ERROR(-112,"js引擎解析错误"),
;

    private Integer code;

    private String msg;

    ExceptionEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
