package com.etone.etonecommon.exception;


public class BusinessException extends RuntimeException{

    private static final long serialVersionUID = 1L;

    private Integer code;  //错误码


    public BusinessException(ExceptionEnum resultEnum) {
        super(resultEnum.getMsg());
        this.code = resultEnum.getCode();
    }
    public BusinessException(String message, Integer code) {
        super(message);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
