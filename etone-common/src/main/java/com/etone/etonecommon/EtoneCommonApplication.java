package com.etone.etonecommon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EtoneCommonApplication {

    public static void main(String[] args) {
        SpringApplication.run(EtoneCommonApplication.class, args);
    }

}
