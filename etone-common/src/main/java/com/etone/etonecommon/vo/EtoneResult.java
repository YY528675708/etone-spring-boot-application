package com.etone.etonecommon.vo;

public class EtoneResult<T> {
    private int code;       //结果代码
    private String msg;      //结果信息
    private T t;        //结果对象

    public EtoneResult() {
    }

    public EtoneResult(int code, String msg, T t) {
        this.code = code;
        this.msg = msg;
        this.t = t;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }
}
