package com.etone.etonecommon.entity;

import io.swagger.annotations.ApiModel;

@ApiModel
public class PageResultVo<T> extends Result<T>{
    //dataTable默认的命令规则
    int pageNumber; //当前页
    long total;//总记录数
    int pageSize;//展示的记录数



    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }


}
