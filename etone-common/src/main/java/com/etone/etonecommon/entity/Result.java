package com.etone.etonecommon.entity;

/**
 * 包装返回结果的实体类
 * @param <T>
 */
public class Result<T> {



    //    error_code 状态值：0 极为成功，其他数值代表失败
    private Integer code;

    //    error_msg 错误信息，若status为0时，为success
    private String result;

    //    content 返回体报文的出参，使用泛型兼容不同的类型
    private T content;

    private String errorStrack;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public T getData(Object object) {
        return content;
    }

    public void setContent(T content) {
        this.content = content;
    }

    public T getContent() {
        return content;
    }


    public String getErrorStrack() {
        return errorStrack;
    }

    public void setErrorStrack(String errorStrack) {
        this.errorStrack = errorStrack;
    }

    @Override
    public String toString() {
        return "Result{" +
                "code=" + code +
                ", result='" + result + '\'' +
                ", content=" + content +
                ", errorStrack='" + errorStrack + '\'' +
                '}';
    }
}